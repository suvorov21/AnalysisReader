#include "AnalysisReader.hxx"

int main(int argc, char *argv[]){

  AnalysisReader* reader = new AnalysisReader(argc, argv);
  reader->Read();

  return 0;
}