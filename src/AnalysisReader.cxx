//
#include "AnalysisReader.hxx"
#include "Utils.h"

// c++
#include <sstream>

// ROOT
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"

using namespace std;

const int MaxPart = 50;

AnalysisReader::AnalysisReader(int argc, char *argv[]) {
  _file_name = "/t2k/users/suvorov/dev/FileList/runs/MC_neut_run3_sand.list";

  _out_file_path = "";
  _out_file_name = "gRooTracher_gamma_4w_";

  _debug =0;
  _par_ID = 13;
  _first_file = 0;
  _last_file  = 100000;


  for (;;) {
    int c = getopt(argc, argv, "b:s:i:p:o:");
    if (c < 0) break;
    switch (c) {
      case 'b' : _first_file      = atoi(optarg);                   break;
      case 's' : _last_file       = _first_file + atoi(optarg);   break;
      case 'i' : _file_name       = optarg;                         break;
      case 'p' : _par_ID          = atoi(optarg);                   break;
      case 'o' : _out_file_name   = optarg;                         break;
    }
  }
}

bool AnalysisReader::Read() {
  std::ifstream fList0(_file_name.Data());
  // count files for progress bar
  Int_t N_lines = 0;
  while(fList0.good()) {
    std::string file_name;
    getline(fList0, file_name);
    ++N_lines;
  }


  std::ifstream fList(_file_name.Data());
  Int_t fileID = 0;

  cout << "Start process over " << _file_name << endl;
  cout << "Read from " << _first_file << " to " << _last_file << endl;
  cout << "Write to " << _out_file_path + _out_file_name << endl;


  cout << "[                                                  ] N = " << N_lines << "\r[";
  if(fList.good()) {
    while(fList.good()) {
      std::string file_name;
      getline(fList, file_name);
      if(fList.eof()) break;
      ++fileID;

      if (fileID <= _first_file || fileID > _last_file)
        continue;
      if (_debug)
        cout << "Running over " << fileID << "   from " << N_lines << endl;
      if ((fileID - _first_file) %((_last_file - _first_file)/50) == 0)
        cout << "." << flush;
      Run(file_name, fileID);

    }
  }

  cout << "] N = " << N_lines;

  cout << "  Success" << endl;

  return true;
}

bool AnalysisReader::Run(TString file_name, Int_t fileID) {

  _in_file      = new TFile(file_name.Data(), "READ");
  _truth        = (TTree*)_in_file->Get("/TruthDir/Trajectories");
  _truth_vertex = (TTree*)_in_file->Get("/TruthDir/Vertices");
  _Ntree        = (TTree*)_in_file->Get("/TruthDir/NRooTrackerVtx");

  // POT stuff
  _header  = (TTree*)_in_file->Get("/HeaderDir/BasicHeader");
  _beam_sum = (TTree*)_in_file->Get("/HeaderDir/BeamSummaryData");


  // declare input vars
  Int_t RunID, SubrunID, EventID;
  Int_t NTraj, NTrajBaryon, NTrajPhoton;
  TClonesArray *Trajectories = new TClonesArray("ND::TTruthTrajectoriesModule::TTruthTrajectory", 100);

  fVertices = new TClonesArray("ND::TTruthVerticesModule::TTruthVertex", 100);
  _truth_vertex->SetBranchAddress("NVtx",     &fNVertices);
  _truth_vertex->SetBranchAddress("Vertices", &fVertices);

  Int_t NNVtx;
  TClonesArray *NVtx = new TClonesArray("ND::NRooTrackerVtx", 100);

  Int_t Spill;
  Float_t POTPerSpill;

  _header->SetBranchAddress("Spill",        &Spill);
  _header->SetBranchAddress("POTPerSpill",  &POTPerSpill);

  Int_t ND280Spill;
  _beam_sum->SetBranchAddress("ND280Spill", &ND280Spill);

  // declare input branches
  _truth->SetBranchAddress("RunID",        &RunID);
  _truth->SetBranchAddress("SubrunID",     &SubrunID);
  _truth->SetBranchAddress("EventID",      &EventID);
  _truth->SetBranchAddress("NTraj",        &NTraj);
  _truth->SetBranchAddress("NTrajBaryon",  &NTrajBaryon);
  _truth->SetBranchAddress("NTrajPhoton",  &NTrajPhoton);
  _truth->SetBranchAddress("Trajectories", &Trajectories);

  _Ntree->SetBranchAddress("NVtx",         &NNVtx);
  _Ntree->SetBranchAddress("Vtx",          &NVtx);

  // set output
  TString file_path = _out_file_path + _out_file_name;
  file_path += fileID;
  file_path += ".root";
  _out_file = new TFile(file_path.Data(), "RECREATE");
  _out_file->cd();
  _out_tree = new TTree("gRooTracker", "");

  fEvtFlags_ = NULL;
  fEvtCode_ = NULL;

  fEvtNum_ = 0;

  stringstream ss;
  ss << MaxPart;
  string str = ss.str();

  _out_tree->Branch("EvtFlags",       &fEvtFlags_);
  _out_tree->Branch("EvtCode",        &fEvtCode_);
  _out_tree->Branch("EvtNum",         &fEvtNum_,      "fEvtNum_/I");
  _out_tree->Branch("EvtXSec",        &fEvtXSec_);
  _out_tree->Branch("EvtDXSec",       &fEvtDXSec_);
  _out_tree->Branch("EvtWght",        &fEvtWght_);
  _out_tree->Branch("EvtProb",        &fEvtProb_);
  _out_tree->Branch("EvtVtx",          fEvtVtx_,      "fEvtVtx_[4]/D");
  _out_tree->Branch("StdHepN",        &fStdHepN_,     "fStdHepN_/I");
  _out_tree->Branch("StdHepPdg",       fStdHepPdg_,     "fStdHepPdg_[4]/I");
  _out_tree->Branch("StdHepStatus",    fStdHepStatus_,    "fStdHepStatus_[4]/I");
  _out_tree->Branch("StdHepX4",        fStdHepX4_,      ("fStdHepX4_[" + str + "][4]/D").c_str());
  _out_tree->Branch("StdHepP4",        fStdHepP4_,      ("fStdHepP4_[" + str + "][4]/D").c_str());
  _out_tree->Branch("StdHepPolz",      fStdHepPolz_,    ("fStdHepPolz_[" + str + "][3]/D").c_str());
  _out_tree->Branch("StdHepFd",        fStdHepFd_,      ("fStdHepFd_[" + str + "]/I").c_str());
  _out_tree->Branch("StdHepLd",        fStdHepLd_,      ("fStdHepLd_[" + str + "]/I").c_str());
  _out_tree->Branch("StdHepFm",        fStdHepFm_,      ("fStdHepFm_[" + str + "]/I").c_str());
  _out_tree->Branch("StdHepLm",        fStdHepLm_,      ("fStdHepLm_[" + str + "]/I").c_str());
  _out_tree->Branch("NuParentPdg",    &fNuParentPdg_);
  _out_tree->Branch("NuParentDecMode",&fNuParentDecMode_);
  _out_tree->Branch("NuParentDecP4",   fNuParentDecP4_,   "fNuParentDecP4_[4]/D");
  _out_tree->Branch("NuParentDecX4",   fNuParentDecX4_,   "fNuParentDecX4_[4]/D");
  _out_tree->Branch("NuParentProP4",   fNuParentProP4_,   "fNuParentProP4_[4]/D");
  _out_tree->Branch("NuParentProX4",   fNuParentProX4_,   "fNuParentProX4_[4]/D");
  _out_tree->Branch("NuParentProNVtx",&fNuParentProNVtx_);

  _info_tree = new TTree("header", "");
  events_     = 0;
  total_POT_  = 0;
  _info_tree->Branch("events",          &events_);
  _info_tree->Branch("POT_per_file",    &total_POT_);

  TH1D* dist = new TH1D("dist", "", 200, 0., 2000.);
  TH2D* dist_e = new TH2D("dist_e", "", 200, 0., 2000., 200., 0., 200);
  // define the geometry
  // SFGD
  TVector3 SFGD_pos(0., 0., -1707.);
  utils::TBox3D_AAB* SFGD_box = new utils::TBox3D_AAB(SFGD_pos, 960., 280., 960.);

  // P0D
  // Float_t p0dmin[3] = {-1092.79,-1107.39,-3296.48};
  // Float_t p0dmax[3] = {1012.45,1130.99,-938.753};
  // based on ND280 MC code US P0D ECal take place until -3000
  // OLD CODE with all P0D included
  // TVector3 P0D_pos(-40.17, 11.8, -2117.6165);
  // utils::TBox3D_AAB* P0D_box = new utils::TBox3D_AAB(P0D_pos, 1052.62, 1119.19, 1178.8635);
  // NEW CODE w/o P0D ECal
  TVector3 P0D_pos(-40.17, 11.8, -1969.3765);
  utils::TBox3D_AAB* P0D_box = new utils::TBox3D_AAB(P0D_pos, 1052.62, 1119.19, 1030.6235);

  float bunch_offset = 2690.;
  float bunch[8] = {2750.20, 3332.00, 3914.70, 4497.00, 5078.40, 5659.70, 6243.40, 6824.20};
  for (int i = 0; i < 8; ++i) {
    bunch[i] -= bunch_offset;
  }

  double bunch_sep[9];
  bunch_sep[0] = -1000;
  for (int i = 1; i < 8; ++i) {
    bunch_sep[i] = bunch[i-1] + 0.5 * (bunch[i] - bunch[i-1]);
  }
  bunch_sep[8] = 10000.;

  TAxis* bunch_ax = new TAxis(8, bunch_sep);

  Int_t Nevt = _truth->GetEntries();
  for (Int_t eventID = 0; eventID < Nevt; ++eventID) {
    _truth->GetEntry(eventID);
    _truth_vertex->GetEntry(eventID);
    _Ntree->GetEntry(eventID);
    _header->GetEntry(eventID);

    total_POT_ += POTPerSpill;

    vector<vector<TLorentzVector> > traj_pos;
    vector<vector<TLorentzVector> > traj_mom;

    vector<Int_t> PrimVertexID;
    vector<TLorentzVector> ini_pos;

    if (_debug > 2) {
      cout << "**************************************************************" << endl;
      cout << "Event   " << RunID << "   " << SubrunID << "   " <<  EventID << endl;
      cout << "**************************************************************" << endl;
    }

    if (!NNVtx)
      continue;

    ND::NRooTrackerVtx* gvtx = (ND::NRooTrackerVtx*)(*NVtx)[0];

    if (_debug > 0)
      cout << "Event start " << eventID << endl;

    for (Int_t trajID = 0; trajID < NTraj; ++trajID) {

      ND::TTruthTrajectoriesModule::TTruthTrajectory* traj =
        (ND::TTruthTrajectoriesModule::TTruthTrajectory*)(*Trajectories)[trajID];

      // look at neutrons
      if (traj->PDG != _par_ID) continue;

      if (_debug > 1){
        cout << "--------------------" << endl;
        cout << "Neutron with Mom  " << traj->InitMomentum.P() << endl;
      }

      utils::TRay traj_ray(traj->InitPosition.Vect(), traj->InitMomentum.Vect());

      Double_t t_min, t_max;
      if (!SFGD_box->Intersect(traj_ray, t_min, t_max))
        continue;

      if (t_min < 0 || t_max < 0)
        continue;

      // trajectories from P0D are ommited
      if (P0D_box->IsInside(traj->InitPosition.Vect()))
        continue;

      Double_t t_fin;
      traj_ray.Project(traj->FinalPosition.Vect(), t_fin);
      Double_t t_min_P0D, t_max_P0D;
      P0D_box->Intersect(traj_ray, t_min_P0D, t_max_P0D);

      // ommit trajectories stopped before P0D
      if (t_fin < t_min_P0D)
        continue;

      if (_debug > 0) {
        cout << "Find track with mom = " << traj->InitMomentum.P() << endl;;
        cout << "From   ";
        printVect(traj->InitPosition.Vect());
        cout << endl;
        cout << "To     ";
        printVect(traj->FinalPosition.Vect());
        cout << endl;
        cout << "vec :" << endl;
        printVect(traj->InitMomentum.Vect());
        cout << endl;
        cout << "Intersect SFGD at:" << endl;
        printVect(traj_ray.Propagate(t_min));
        cout << endl;
        printVect(traj_ray.Propagate(t_max));
        cout << endl;
      }

      // timing propogation
      Int_t primID = traj->PrimaryID;
      Float_t prim_time = 0;
      for (Int_t trajID2 = 0; trajID2 < NTraj; ++trajID2) {
        ND::TTruthTrajectoriesModule::TTruthTrajectory* traj2 =
        (ND::TTruthTrajectoriesModule::TTruthTrajectory*)(*Trajectories)[trajID2];
        if (traj2->ID == primID) {
          prim_time = traj2->InitPosition.T();
          break;
        }
      }

      Int_t bunch_ID = bunch_ax->FindBin(prim_time);
      if (bunch_ID < 1 || bunch_ID > 8) {
        std::cout << "ERROR! bunch number is out of bounds = " << bunch_ID << "\tprim_time = " << prim_time << std::endl;
        continue;
      }

      float time_bias = bunch[bunch_ID - 1];
      Double_t time = traj->InitPosition.T() + t_min / (traj->InitMomentum.Beta() * units::clight) - time_bias + 200;

      if (_debug > 0)
        cout << "Looking for prim vertex" << endl;
      ND::TTruthVerticesModule::TTruthVertex* PrimVtx = GetVertexOfPrimaryTrajectory(traj->PrimaryID);
      if (_debug > 0)
        cout << "Looking for vertex in array" << endl;

      int index = 0;
      if (!PrimVtx) {
        if (PrimVertexID.size() == 0) {
          PrimVertexID.push_back(0);
          index = 0;
          traj_pos.resize(traj_pos.size() + 1);
          traj_mom.resize(traj_mom.size() + 1);
        } else {
          index = distance(PrimVertexID.begin(), PrimVertexID.end() - 1);
        }
        //cerr << "No prim vertex!!!  " << endl;
        //exit(1);
      } else {
        vector<Int_t>::iterator it = find(PrimVertexID.begin(), PrimVertexID.end(), PrimVtx->ID);
        if (it == PrimVertexID.end()) {
          if (_debug > 0)
            cout << "Add prim vertex " << PrimVtx->ID << endl;
          PrimVertexID.push_back(PrimVtx->ID);
          ini_pos.push_back(PrimVtx->Position);
          it = PrimVertexID.end() - 1;
          if (_debug > 0)
            cout << "Resizing arrays" << endl;
          traj_pos.resize(traj_pos.size() + 1);
          traj_mom.resize(traj_mom.size() + 1);

          if (_debug > 0)
            cout << "New vector size " << traj_pos.size() << endl;
        }
        index = distance(PrimVertexID.begin(), it);
      }

      if (_debug > 0)
        cout << "Fillind mom" << endl;
      traj_pos[index].push_back(TLorentzVector(traj_ray.Propagate(t_min), time));
      traj_mom[index].push_back(TLorentzVector(traj->InitMomentum));

      Double_t mom = traj->InitMomentum.P();
      Double_t ene = sqrt(traj->InitMomentum.M2() + mom*mom);

      dist->Fill(t_max - t_min);
      dist_e->Fill(t_max - t_min, ene - traj->InitMomentum.M());
    } // loop over trajs

    for (uint vertID = 0; vertID < traj_pos.size(); ++vertID) {

      if (traj_pos[vertID].size() < 1)
        continue;

      ++events_;

      // fill common vars
      fEvtCode_         = gvtx->EvtCode;
      fEvtXSec_         = gvtx->EvtXSec;
      fEvtDXSec_        = gvtx->EvtDXSec;
      fEvtWght_         = gvtx->EvtWght;
      fEvtProb_         = gvtx->EvtProb;
      fNuParentPdg_     = gvtx->NuParentPdg;
      fNuParentDecMode_ = gvtx->NuParentDecMode;
      fNuParentDecP4_[0]   = gvtx->NuParentDecP4[0];
      fNuParentDecP4_[1]   = gvtx->NuParentDecP4[1];
      fNuParentDecP4_[2]   = gvtx->NuParentDecP4[2];
      fNuParentDecP4_[3]   = gvtx->NuParentDecP4[3];

      if (ini_pos.size() == 0 || ini_pos.size()-1 < vertID) {
        fNuParentDecX4_[0]   = gvtx->NuParentDecX4[0];
        fNuParentDecX4_[1]   = gvtx->NuParentDecX4[1];
        fNuParentDecX4_[2]   = gvtx->NuParentDecX4[2];
        fNuParentDecX4_[3]   = gvtx->NuParentDecX4[3];
      } else {
        fNuParentDecX4_[0]   = ini_pos[vertID].X();
        fNuParentDecX4_[1]   = ini_pos[vertID].Y();
        fNuParentDecX4_[2]   = ini_pos[vertID].Z();
        fNuParentDecX4_[3]   = ini_pos[vertID].T();
      }

      fNuParentProP4_[0]   = gvtx->NuParentProP4[0];
      fNuParentProP4_[1]   = gvtx->NuParentProP4[1];
      fNuParentProP4_[2]   = gvtx->NuParentProP4[2];
      fNuParentProP4_[3]   = gvtx->NuParentProP4[3];
      fNuParentProX4_[0]   = gvtx->NuParentProX4[0];
      fNuParentProX4_[1]   = gvtx->NuParentProX4[1];
      fNuParentProX4_[2]   = gvtx->NuParentProX4[2];
      fNuParentProX4_[3]   = gvtx->NuParentProX4[3];

      // fill event vars
      fStdHepN_            = traj_pos[vertID].size() + 2;
      fEvtVtx_[0]          = traj_pos[vertID][0].X() / 1000.;
      fEvtVtx_[1]          = traj_pos[vertID][0].Y() / 1000.;
      fEvtVtx_[2]          = traj_pos[vertID][0].Z() / 1000.;
      fEvtVtx_[3]          = traj_pos[vertID][0].T() / 1.E9;

      fStdHepPdg_[0] = 14;
      fStdHepPdg_[1] = 1000180400;
      fStdHepPdg_[2] = _par_ID;

      fStdHepP4_[0][0] = 0.;
      fStdHepP4_[0][1] = 0.;
      fStdHepP4_[0][2] = 0.7;
      fStdHepP4_[0][3] = 0.7;

      fStdHepP4_[1][0] = 0;
      fStdHepP4_[1][1] = 0;
      fStdHepP4_[1][2] = 0;
      fStdHepP4_[1][3] = 40;

      fStdHepStatus_[0] = 0;
      fStdHepStatus_[1] = 0;
      fStdHepStatus_[2] = 1;

      if (_debug> 0)
        cout << "Fill 1st" << endl;

      fStdHepP4_[2][0] = traj_mom[vertID][0].Px() / 1000.;
      fStdHepP4_[2][1] = traj_mom[vertID][0].Py() / 1000.;
      fStdHepP4_[2][2] = traj_mom[vertID][0].Pz() / 1000.;
      fStdHepP4_[2][3] = traj_mom[vertID][0].E() / 1000.;

      for (UInt_t trajID = 1; trajID < traj_pos[vertID].size(); ++trajID) {
        if (_debug> 0)
          cout << "Fill other " << trajID << endl;
        fStdHepPdg_[trajID+2] = _par_ID;
        fStdHepStatus_[trajID+2] = 1;
        fStdHepP4_[trajID+2][0] = traj_mom[vertID][trajID].Px() / 1000.;
        fStdHepP4_[trajID+2][1] = traj_mom[vertID][trajID].Py() / 1000.;
        fStdHepP4_[trajID+2][2] = traj_mom[vertID][trajID].Pz() / 1000.;
        fStdHepP4_[trajID+2][3] = traj_mom[vertID][trajID].E() / 1000.;

        fStdHepX4_[trajID+2][0] = (traj_pos[vertID][trajID].X() - traj_pos[vertID][0].X()) * 1.E12;
        fStdHepX4_[trajID+2][1] = (traj_pos[vertID][trajID].Y() - traj_pos[vertID][0].Y()) * 1.E12;
        fStdHepX4_[trajID+2][2] = (traj_pos[vertID][trajID].Z() - traj_pos[vertID][0].Z()) * 1.E12;
        fStdHepX4_[trajID+2][3] = (traj_pos[vertID][trajID].T() - traj_pos[vertID][0].T()) / 1.E9 ;
      }
      _out_tree->Fill();
      ++fEvtNum_;
    } // loop over verticies in event
  } // loop over events

  _info_tree->Fill();

  _out_tree->Write("", TObject::kOverwrite);
  dist->Write("", TObject::kOverwrite);
  dist_e->Write("", TObject::kOverwrite);
  _info_tree->Write("", TObject::kOverwrite);
  _out_file->Close();

  _in_file->Close();

  return true;
}


ND::TTruthVerticesModule::TTruthVertex* AnalysisReader::GetVertexOfPrimaryTrajectory(Int_t primary_trajectory_id) const
{
  if( fVertices == 0 || fNVertices == 0 )
  {
    std::cerr << "[ERROR][TTruthUtils] Vertices clones array / number not available" << std::endl;
    return 0;
  }

  // Check for common 'not set' type values
  if( primary_trajectory_id < 0 ) return NULL;

  ND::TTruthVerticesModule::TTruthVertex* vtx;

  for(Int_t i = 0; i < fNVertices; ++i)
  {
    vtx = (ND::TTruthVerticesModule::TTruthVertex*)(*fVertices)[i];

    for(unsigned int j = 0; j != vtx->PrimaryTrajIDs.size(); ++j)
    {
      if( vtx->PrimaryTrajIDs.at(j) == primary_trajectory_id ) return vtx;
    }
  }

  return 0;
}