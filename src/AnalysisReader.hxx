#ifndef analysys_reader_hxx
#define analysis_reader_hxx

#include "TString.h"
#include "oaAnalysisReaderProjectHeaders.h"
#include "TFile.h"
#include "TTree.h"

class TBits;
class TObjString;
class TString;

class AnalysisReader {
public:
  AnalysisReader(int argc, char *argv[]);
  virtual ~AnalysisReader(){;};

  bool Read();
  bool Run(TString file_name, Int_t fileID);

  ND::TTruthVerticesModule::TTruthVertex* GetVertexOfPrimaryTrajectory(Int_t primary_trajectory_id) const;

private:
  Int_t _debug;

  TString _file_name;
  Int_t _first_file;
  Int_t _last_file;

  TFile* _in_file;
  TTree* _truth;
  TTree* _truth_vertex;
  TTree* _Ntree;
  TTree* _header;
  TTree* _beam_sum;

  Int_t fNVertices;
  TClonesArray* fVertices;

  TString _out_file_path;
  TString _out_file_name;
  TFile* _out_file;
  TTree* _out_tree;
  TTree* _info_tree;

  Int_t events_;
  Float_t total_POT_;

  Int_t _par_ID;

  /// The generator-specific event flags.
  TBits*       fEvtFlags_;

  /// The generator-specific string with the 'event code'
  TObjString*  fEvtCode_;

  /// The sequence number of the event (the event number).
  int         fEvtNum_;

  /// The cross section for the event (1E-38 cm2)
  double      fEvtXSec_;

  /// The differential cross section for the event kinematics
  /// (1E-38 cm2/{K^n})
  double      fEvtDXSec_;

  /// The weight for the event
  double      fEvtWght_;

  /// The probability for the event (given the cross section, path lengths,
  /// etc.).
  double      fEvtProb_;

  /// The event vertex position in detector coord syst (in meters and
  /// seconds).
  double      fEvtVtx_[4];

  /// The number of particles in the particle arrays to track
  int         fStdHepN_;

  /// The maximum number of particles that can be in the particle arrays.
  static const int kNPmax = 1000;

  /// The PDG codes for the particles to track.  This may include generator
  /// specific codes for pseudo particles.
  int         fStdHepPdg_[kNPmax]; //[fStdHepN]

  /// The a generator specific status for each particle.  Particles with a
  /// status equal to 1 will be tracked.
  ///
  /// The HEPEVT status values are as follows:
  /// - 0 : null entry.
  /// - 1 : an existing entry, which has not decayed or fragmented. This is
  ///    the main class of entries, which represents the `final state' given
  ///    by the generator.
  /// - 2 : an entry which has decayed or fragmented and is therefore not
  ///    appearing in the final state, but is retained for event history
  ///    information.
  /// - 3 : a documentation line, defined separately from the event
  ///    history. This could include the two incoming reacting particles,
  ///    etc.
  /// - 4 to 10 :
  ///    undefined, but reserved for future standards.
  /// - 11 to 200 : at the disposal of each model builder for constructs
  ///    specific to his program, but equivalent to a null line in the
  ///    context of any other program.
  /// - 201 and above : at the disposal of users, in particular for event
  /// tracking in the detector.
  ///
  /// The GENIE generator approximately follows the HEPEVT status codes.
  /// As of July 2008, the status values found the GENIE source code are:
  ///   - -1 -- Undefined particle
  ///   -  0 -- An initial state particle.InformationalVertex
  ///   -  1 -- A stable final state particle to be tracked.
  ///   -  2 -- An intermediate particle that should not be tracked.
  ///   -  3 -- A particle which decayed and should not be tracked.  If
  ///            this particle produced daughters to be tracked, they will
  ///            have a state of 1.
  int         fStdHepStatus_[kNPmax]; //[fStdHepN]

  /// The position (x, y, z, t) (fm, second) of the particle in the nuclear
  /// frame
  double      fStdHepX4_[kNPmax][4]; //[fStdHepN]

  /// The 4-momentum (px, py, pz, E) of the particle in the LAB frame (GeV)
  double      fStdHepP4_[kNPmax][4]; //[fStdHepN]

  /// The particle polarization vector.
  double      fStdHepPolz_  [kNPmax][3]; //[fStdHepN]

  /// The index of the first daughter of the particle in the arrays.
  int         fStdHepFd_[kNPmax]; //[fStdHepN]

  /// The index last daughter of the particle in the arrays.
  int         fStdHepLd_[kNPmax]; //[fStdHepN]

  /// The index of the first mother of the particle in there arrays.
  int         fStdHepFm_[kNPmax]; //[fStdHepN]

  /// The index of the last mother of the particle in the arrays.
  int         fStdHepLm_[kNPmax]; //[fStdHepN]

  //////////////////////////////
  /// The following variables are copied more or less directly from the
  /// input flux generator.
  //////////////////////////////

  /// The PDG code of the particle which created the parent neutrino.
  int         fNuParentPdg_;

  /// The interaction mode at the vertex which created the parent neutrino.
  /// This is normally the decay mode of the parent particle.
  int         fNuParentDecMode_;

  /// The 4 momentum of the particle at the vertex which created the parent
  /// neutrino.  This is normally the momentum of the parent particle at the
  /// decay point.
  double      fNuParentDecP4_[4];

  /// The position of the vertex at which the neutrino was created.  This is
  /// passed directly from the beam (or other flux) generator, and is in the
  /// native units of the original generator.
  double      fNuParentDecX4_[4];

  /// The momentum of the parent particle at it's production point.  This is
  /// in the native energy units of the flux generator.
  double      fNuParentProP4_[4];

  /// The position of the parent particle at it's production point.  This
  /// uses the target as the origin and is in the native units of the flux
  /// generator.
  double      fNuParentProX4_[4];

  /// The vertex ID of the parent particle vertex.
  int         fNuParentProNVtx_;
  };

#endif