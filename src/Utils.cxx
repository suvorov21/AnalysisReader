#include "Utils.h"

#include <cmath>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include "TMath.h"

namespace utils {

  /*!
   * ctor
   */
  TRay::TRay(const TVector3& pos, const TVector3& dir){
    dir__ = dir.Unit();
    pos__ = pos;

    //inverse direction
    Double_t x = dir__.X()!=0 ? 1/dir__.X() : DBL_MAX;
    Double_t y = dir__.Y()!=0 ? 1/dir__.Y() : DBL_MAX;
    Double_t z = dir__.Z()!=0 ? 1/dir__.Z() : DBL_MAX;

    invDir__.SetXYZ(x,y,z);


  }

  TVector3 TRay::Project(const TVector3& r0, Double_t& t_proj) const {
    TVector3 proj_dir = r0 - pos__;

    t_proj = proj_dir.Dot(dir__);

    return Propagate(t_proj);
  }

  /*!
   * ctor
   */
  TBox3D_AAB::TBox3D_AAB(const TVector3& pos, Double_t Dx, Double_t Dy, Double_t Dz ){
    pos__   = pos;

    Dx__  = fabs(Dx);
    Dy__  = fabs(Dy);
    Dz__  = fabs(Dz);

    // get the edges
    min__ = TVector3(pos - TVector3(Dx__, Dy__, Dz__));
    max__ = TVector3(pos + TVector3(Dx__, Dy__, Dz__));
  }

  /*!
   * Whether this box is intersected by a given ray, return two param corresponding to intersection
   */

  bool TBox3D_AAB::Intersect(const TRay& ray, Double_t& t_min, Double_t& t_max) const {

    t_min = units::kUnassigned;
    t_max = units::kUnassigned;

    Double_t tmin, tmax, tymin, tymax, tzmin, tzmax;

    // X
    tmin = (min__.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    tmax = (max__.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    if (tmin > tmax) std::swap(tmin, tmax);

    // Y
    tymin = (min__.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();
    tymax = (max__.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();

    if (tymin > tymax) std::swap(tymin, tymax);
    if ((tmin > tymax) || (tymin > tmax))
      return false;

    if (tymin > tmin)
      tmin = tymin;
    if (tymax < tmax)
      tmax = tymax;

    // Z
    tzmin = (min__.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    tzmax = (max__.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    if (tzmin > tzmax) std::swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
      return false;

    if (tzmin > tmin)
      tmin = tzmin;
    if (tzmax < tmax)
      tmax = tzmax;

    //set the output values
    t_min =  tmin;

    t_max = tmax;

    return true;
  }

  bool TBox3D_AAB::IsInside(const TVector3& r0) const {

    if (r0.Z() > max__.Z() || r0.Z() < min__.Z())
      return false;
    else if (r0.Y() > max__.Y() || r0.Y() < min__.Y())
      return false;
    else if (r0.X() > max__.X() || r0.X() < min__.X())
      return false;

    return true;
  }
} //namespace
