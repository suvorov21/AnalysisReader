#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <sstream>
#include <map>


//ROOT
#include <TVector3.h>
#include <TLorentzVector.h>
#include <utility>
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TString.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"

namespace units {
  const Int_t kUnassigned = 0xDEAD;
  const double clight = 299.792458; // mm/ns
}

#define _(par) std::cout << #par <<" value "<< par <<std::endl;
#define stringify(name) #name
#define printVect(vect) std::cout << vect.X() << "    " << vect.Y() << "   " << vect.Z()

namespace utils {
  class TRay{
    public:
      /// a constructor given a position and direction
      TRay(const TVector3&, const TVector3&);

      /// a default destructor
      virtual ~TRay() {};

      const TVector3& GetPos()    const {return pos__;}
      const TVector3& GetDir()    const {return dir__;}
      const TVector3& GetInvDir() const {return invDir__;}

      TVector3 Propagate(Double_t distance) const {return pos__ + distance*dir__;}

      TVector3 Project(const TVector3&, Double_t&) const;


    private:
      TVector3 pos__;
      TVector3 dir__;
      TVector3 invDir__;
  };

  /// this is a simple box in 3D, for the moment axis aligned one (as is ND280 complex)
  class TBox3D_AAB{
    public:
      /// a constructor given a position of the center and half-sizes for each side
      TBox3D_AAB(const TVector3&, Double_t, Double_t, Double_t);

      /// default destructor
      virtual ~TBox3D_AAB() {};

      const TVector3& GetPos() const {return pos__;}

      Double_t GetDx() const {return Dx__;}
      Double_t GetDy() const {return Dy__;}
      Double_t GetDz() const {return Dz__;}

      const TVector3& GetMin() const {return min__;}
      const TVector3& GetMax() const {return max__;}

      /// wether a ray intersect this box
      bool Intersect(const TRay&, Double_t&, Double_t&) const;
      bool IsInside(const TVector3&) const;

    private:

      /// position of the center
      TVector3 pos__;

      /// half sizes
      Double_t Dx__;
      Double_t Dy__;
      Double_t Dz__;

      /// edge coordinates of the box
      TVector3 min__;
      TVector3 max__;
  };
}

#endif
